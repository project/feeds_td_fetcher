<?php

/**
 * @file
 * Library for td fetcher http requests.
 */

/**
 * Get the content from the given URL.
 *
 * @param string $url
 *   A valid URL (not only web URLs).
 * @param string $username
 *   If the URL uses authentication, supply the username.
 * @param string $password
 *   If the URL uses authentication, supply the password.
 * @param bool $cert
 *   Whether to accept invalid certificates.
 */

/**
 * Intial TD auth http request.
 */
function feeds_td_fetcher_auth_request_get($url, $username = NULL, $password = NULL, $cert = FALSE) {

  // Support the 'feed' and 'webcal' schemes by converting them into 'http'.
  $url = strtr($url, array('feed://' => 'http://', 'webcal://' => 'http://'));

  $curl = http_request_use_curl();

  if ($curl) {
    $headers = array();
    $headers[] = 'User-Agent: Drupal (+http://drupal.org/)';

    $result = new stdClass();
    $result->headers = array();

    // Parse the URL and make sure we can handle the schema.
    // cURL can only support either http:// or https://.
    // CURLOPT_PROTOCOLS is only supported with cURL 7.19.4.
    $uri = parse_url($url);
    if (!isset($uri['scheme'])) {
      $result->error = 'missing schema';
      $result->code = -1002;
    }
    else {
      switch ($uri['scheme']) {
        case 'http':
        case 'https':
          // Valid scheme.
          break;

        default:
          $result->error = 'invalid schema ' . $uri['scheme'];
          $result->code = -1003;
          break;
      }
    }

    if (empty($result->error)) {
      $data_value = json_encode(array("username" => $username, "password" => $password));
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
      curl_setopt($ch, CURLOPT_POST, TRUE);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data_value);
      if ($cert == TRUE) {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      }
      else {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
      }
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      $result = curl_exec($ch);
      if (curl_error($ch)) {
        throw new HRCurlException(
        t('cURL error (@code) @error for @url', array(
          '@code' => curl_errno($ch),
          '@error' => curl_error($ch),
          '@url' => $url,
        )), curl_errno($ch)
        );

      }
    }

    $http = new stdClass();

    $http->code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if (!in_array($http->code, array(200, 201, 202, 203, 204, 205, 206))) {
      throw new Exception(t('Download of @url failed with code !code.', array('@url' => $this->url, '!code' => $result->code)));
    }

    return $result;
  }

}

/**
 * Get the content from the source service URL.
 *
 * @param string $url
 *   A valid URL (not only web URLs).
 * @param string $authorization
 *   Security token passed from the initial td http request.
 * @param bool $cert
 *   Whether to accept invalid certificates.
 */
function feeds_td_fetcher_request_get($url, $authorization, $cert = FALSE) {

  // Support the 'feed' and 'webcal' schemes by converting them into 'http'.
  $url = strtr($url, array('feed://' => 'http://', 'webcal://' => 'http://'));

  $curl = http_request_use_curl();

  if ($curl) {
    $headers = array();
    $headers[] = 'User-Agent: Drupal (+http://drupal.org/)';

    $result = new stdClass();
    $result->headers = array();

    // Parse the URL and make sure we can handle the schema.
    // cURL can only support either http:// or https://.
    // CURLOPT_PROTOCOLS is only supported with cURL 7.19.4.
    $uri = parse_url($url);
    if (!isset($uri['scheme'])) {
      $result->error = 'missing schema';
      $result->code = -1002;
    }
    else {
      switch ($uri['scheme']) {
        case 'http':
        case 'https':
          // Valid scheme.
          break;

        default:
          $result->error = 'invalid schema ' . $uri['scheme'];
          $result->code = -1003;
          break;
      }
    }

    if (empty($result->error)) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
      if ($cert == TRUE) {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      }
      else {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
      }
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      $result = curl_exec($ch);
      if (curl_error($ch)) {
        throw new HRCurlException(
        t('cURL error (@code) @error for @url', array(
          '@code' => curl_errno($ch),
          '@error' => curl_error($ch),
          '@url' => $url,
        )), curl_errno($ch)
        );

      }
    }
  }
  $http = new stdClass();

  $http->code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  if (!in_array($http->code, array(200, 201, 202, 203, 204, 205, 206))) {
    throw new Exception(t('Download of @url failed with code !code.', array('@url' => $this->url, '!code' => $result->code)));
  }
  return $result;
}
