<?php

/**
 * @file
 * Class module for implementing TD fetcher.
 */

/**
 * Result of FeedsHTTPFetcherAppendHeaders::fetch().
 */
class FeedsTDFetcherResult extends FeedsFetcherResult {
  protected $url;
  protected $auth;
  protected $cert;

  /**
   * Constructor.
   */
  public function __construct($url = NULL, $auth = NULL, $cert = FALSE) {
    $this->url = $url;
    $this->auth = $auth;
    $this->cert = $cert;
    parent::__construct('');
  }

  /**
   * Overide parent:getRaw()
   */
  public function getRaw() {

    feeds_include_library('http_request.inc', 'http_request');
    module_load_include('inc', 'feeds_td_fetcher', 'libraries/feeds_td_fetcher_request_get');

    $result = feeds_td_fetcher_request_get($this->url, $this->auth, $this->cert);

    return $result;
  }

}

/**
 * Fetches data via HTTP and appending custom headers.
 */
class FeedsTDFetcher extends FeedsFetcher {

  /**
   * Implements FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $auth_inc_path = drupal_get_path('module', 'feeds_td_fetcher') . '/libraries/feeds_td_fetcher_request_get.inc';

    require_once $auth_inc_path;
    feeds_include_library('http_request.inc', 'http_request');

    $source_config = $source->getConfigFor($this);

    $username = $this->config['username'];
    $password = $this->config['password'];
    $cert = $this->config['test_mode'];
    $auth_result = feeds_td_fetcher_auth_request_get(
      $this->config['login_url'],
      $username,
      $password,
      $cert
    );
    if (!empty($auth_result)) {
      $auth_feed = "Authorization: Bearer " . $auth_result;
    }

    return new FeedsTDFetcherResult(
      $source_config['source'],
      $auth_feed,
      $cert
    );

  }

  /**
   * Override parent::request().
   */
  public function request($feed_nid = 0) {
    feeds_dbg($_GET);
    @feeds_dbg(file_get_contents('php://input'));
    try {
      feeds_source($this->id, $feed_nid)->existing()->import();
    }
    catch (Exception $e) {
      // In case of an error, respond with a 503.
      drupal_add_http_header('Status', '503 Service unavailable');
      drupal_exit();
    }

    // Will generate the default 200 response.
    drupal_add_http_header('Status', '200 OK');
    drupal_exit();

  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
      'login_url' => '',
      'request_timeout' => NULL,
      'test_mode' => FALSE,
      'username' => '',
      'password' => '',
    );
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = array();

    $form['test_mode'] = array(
      '#type' => 'checkbox',
      '#title' => t('Test mode'),
      '#description' => t('Applies certain settings for test mode sets CURLOPT_SSL_VERIFYPEER FALSE'),
      '#default_value' => $this->config['test_mode'],
    );

    $form['login_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Login Url'),
      '#description' => t('Enter the full URL to authenticate your request.'),
      '#default_value' => $this->config['login_url'],
      '#required' => TRUE,
    );
    $form['username'] = array(
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#description' => t('Enter your API Username'),
      '#default_value' => $this->config['username'],
      '#size' => 15,
      '#required' => TRUE,
    );

    $form['password'] = array(
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#description' => t('Enter your API Password'),
      '#default_value' => $this->config['password'],
      '#size' => 15,
      '#required' => TRUE,
    );
    return $form;
  }

  /**
   * Validating config form fields.
   */
  public function configFormValidate(&$values) {
    if (!feeds_valid_url($values['login_url'], TRUE)) {
      form_set_error('login_url', t('The URL %source is invalid.', array('%source' => $values['login_url'])));
    }

  }

  /**
   * Expose source form.
   */
  public function sourceForm($source_config) {
    $form = array();
    $form['source'] = array(
      '#type' => 'textfield',
      '#title' => t('URL'),
      '#description' => t('Enter a Team Dynamix API Endpoint (Full URL).'),
      '#default_value' => isset($source_config['source']) ? $source_config['source'] : '',
      '#maxlength' => NULL,
      '#required' => TRUE,
    );
    return $form;
  }

  /**
   * Override parent::sourceFormValidate().
   */
  public function sourceFormValidate(&$values) {
    $values['source'] = trim($values['source']);

    if (!feeds_valid_url($values['source'], TRUE)) {
      $form_key = 'feeds][' . get_class($this) . '][source';
      form_set_error($form_key, t('The URL %source is invalid.', array('%source' => $values['source'])));
    }
  }

}
